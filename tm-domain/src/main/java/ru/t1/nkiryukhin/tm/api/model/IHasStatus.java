package ru.t1.nkiryukhin.tm.api.model;

import ru.t1.nkiryukhin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
