package ru.t1.nkiryukhin.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.api.model.ICommand;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.api.service.ITokenService;
import ru.t1.nkiryukhin.tm.enumerated.Role;

@Getter
@Setter
@Component
@NoArgsConstructor
public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @NotNull
    public abstract Role[] getRoles();

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}
