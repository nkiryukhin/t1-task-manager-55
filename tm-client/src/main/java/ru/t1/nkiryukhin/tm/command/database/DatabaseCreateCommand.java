package ru.t1.nkiryukhin.tm.command.database;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.api.service.ILoggerService;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseCreateRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

@Component
public final class DatabaseCreateCommand extends AbstractDatabaseCommand {

    public static final String NAME = "database-create";

    @NotNull
    @Override
    public String getDescription() {
        return "Create database";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATABASE CREATING]");
        @NotNull final DatabaseCreateRequest request = new DatabaseCreateRequest();
        adminEndpoint.createDatabase(request);
        loggerService.info("DATABASE WAS SUCCESSFULLY CREATED");
    }

}
