package ru.t1.nkiryukhin.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM ProjectDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql).setParameter("userId", userId).executeUpdate();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager
                .createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public List<ProjectDTO> findAll(@NotNull String userId, @Nullable Comparator comparator) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId ORDER BY m." + getSortType(comparator);
        return entityManager.createQuery(jpql, ProjectDTO.class).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId.isEmpty()) return 0;
        @NotNull final String jpql = "SELECT COUNT(m) FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class).setParameter("userId", userId).getSingleResult().intValue();
    }

}
